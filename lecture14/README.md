Lecture 14
==========

# Pointers     

Demonstrate with board:

                        Address | Value                         Address | Value
    int  i = 33     [i] 0xF     | 33        *p = 574    [*p][i] 0xF     | 574
    int *p = &i     [p] 0xE     | 0xF                       [p] 0xE     | 0xF

Demonstrate that everything has an address (addresses.c)

Demonstrate that pointer arithmetic (pointers.c)

# Arrays

Demonstrate with board:

                                                                Address | Value
    int a[] = {7, 1, 5};                           *(a + 2)     0xF     | 4
                                                   *(a + 1)     0xE     | 0
                                                   *(a + 0) [a] 0xD     | 9

    a    == &a[0]       # Name of array decays into pointer
    a[x] == *(a + x)                               
    a[x] == *(x + a)
    a[x] == x[a]                

Demonstrate duality of array and pointer (duality.c)

# Strings

Demonstrate with board:

                                                                Address | Value
    char *s = "DOG"                                             0xF     | 0 
                                                                0xE     | G
                                                                0xD     | O
                                                           [*c] 0xC     | D
                                                            [s] 0xB     | 0xC
                                                            [c] 0xA     | 0xC

    for (char *c = s; *c == 0; c++)     ---->   c - s == strlen(s);
