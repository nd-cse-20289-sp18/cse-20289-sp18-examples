Examples 12
===========

# 0. Review: What & How?

1. What is functional programming?

    A programming paradigm that structures programs as a composition of
    functions that are stateless and free of side-effects.

2. How do we perform functional programming in Python?

    A. Higher-Order Functions:                  B. List Comprehensions

    1. map(func, seq)           -> iterator     1. [x for x in seq]              -> list

    2. filter(func, seq)        -> iterator     2. [x for x in seq if condition] -> list

    3. reduce(func, seq, acc)   -> scalar       3. N/A

3. How do we use generators in Python?

        def match(pattern, iterable):
            for item in iterable:
                if re.search(pattern, item):
                    yield item

        matches = match(r'\d{3}', ['abc', '1', '123', 'abc123'])

        print(matches)                          # Discuss: Doesn't do anything

        for m in matches:                       # Discuss: Consumes sequence
            print(m)

    Alternatively:

        def match(pattern, iterable):           # Discuss: Generator expression
            return (item for item in iterable if re.search(pattern, item))

# 1. Discuss: Why & When?

1. Why would we use generators?

    Because generators use lazy evaluation (ie. only produces one item at a
    time), they provide more memory efficiency, which may improve performance.

    That said, there is some overhead in creating and evaluating generators.

2. Why would we use functional programming?

    By structuring problems as a composition of functions we effectively create
    opportunities for concurrency:

        A. Procedural                           B. Functional

        for url in URLS:                        map(wget, urls)
            wget(url)

        Order is implied, and thus sequential   Order is unspecified, so
                                                underlying implementation can do it
                                                in any order

    By being less specific, we free the implementation to do whatever it wants
    (don't micromanage!)

# 2. Concurrency vs Parallelism

    A. Concurrency                              B. Parallelism

    Structuring programs as independent tasks.  Executing tasks simultaneously.

        Example: OS multitasking                    Example: Multi-core multitasking

    There are three main forms of concurrency/parallelism:

1. Task Parallelism: Executing different tasks simultaneously.

        Example: urls generator and map function

2. Data Parallelism: Executing same task on different portions of dataset
   simultaneously.

        Example: map applies function to each element of urls

3. Embarrassingly Parallel: Problems where little or no effort is required to
   parallelize the tasks (usually little communication or synchronization)

        Example: wget all the urls (data independent)

# 3. Parallelization Pitfalls

Parallelization is not aways a good thing

    $ time ./hulk.py -l 4 -c 1 > /dev/null
    $ time ./hulk.py -l 4 -c 4 > /dev/null      # Improvement
    $ time ./hulk.py -l 4 -c 8 > /dev/null      # Discuss: Amdahl's law 
                                                # parallelization limited by serial

    $ time ./hulk.py -l 3 -c 1 > /dev/null
    $ time ./hulk.py -l 3 -c 4 > /dev/null      # Discuss: Overhead

Reasons why not always a good thing:

    1. Overhead of coordinating processes
    2. Contention over resources
    3. Lack of work

# 4. Parallelization Problems (Skit)





1. Contention:      Trying to access same resources

        Example: Students organize pictures from same pile using same table

2. Deadlock:        Everyone is waiting on a resource some other process has

        Example: Student A has picture X but needs picture Y, Student B has
        picture Y but needs picture X

3. Race Condition:  Whomever gets to resource first wins

        Example: Students race to get picture X

Functional programming avoids these problems since it encourages data
parallelism (functions applied to independent portions of data).

# 5. Map-Reduce (Skit)

Want to count the number of each type of picture.

1. Serial:      Have one student count the number of each picture.

2. Concurrent:  Have student break up the problem into independent tasks

    A. Separate pictures into piles (map/shuffle)

    B. Count the totals of each pile (reduce)

3. Parallel:    Have four students

    A. Two separate separate pictures into piles (map/shuffle)

    B. Two count totals of each pile (reduce)

# 6. Map-Reduce (Word Count)

## Map

    import sys

    for line in sys.stdin:
        for word in line.strip().split():
            print('{}\t{}'.format(word, 1))

## Reduce

    import sys

    counts = {}
    for line in sys.stdin:
        k, v  = line.split('\t', 1)
        counts[k] = counts.get(k, 0) + int(v)

    for k, v in sorted(counts.items()):
        print('{}\t{}'.format(k, v))

## Pipeline

    $ ./wc_map.py < README.md | sort | ./wc_reduce.py | awk '{print $2 "\t" $1}' | sort -rn
