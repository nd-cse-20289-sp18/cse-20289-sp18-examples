/* list.c: Deque-based List */

#include "list.h"

#include <assert.h>

/**
 * Create List structure.
 * @param   capacity    Internal capacity.
 * @param   type        Value type.
 * @return  Newly allocated List structure with specified capacity.
 */
List *	list_create(size_t capacity, Type type) {
    List *l = calloc(1, sizeof(List));
    if (l) {
        l->type     = type;
        l->capacity = capacity;
        l->data     = calloc(l->capacity, sizeof(Value));
        if (l->data == NULL) {
            free(l);
            return NULL;
        }
    }
    return l;
}

/**
 * Delete List structure.
 * @param   l           List structure.
 * @return  NULL.
 */
List *	list_delete(List *l) {
    for (List *curr = l, *next = NULL; curr; curr = next) {
        next = curr->next;
        free(curr->data);
        free(curr);
    }
    return NULL;
}

/**
 * Search List structure.
 * @param   l           List structure.
 * @param   value       Value to search for.
 * @return  Whether or not the value is in the List structure.
 */
bool    list_search(const List *l, const Value value) {
    for (const List *curr = l; curr; curr = curr->next) {
        for (size_t v = 0; v < curr->size; v++) {
            if (curr->data[v].number == value.number)
                return true;
        }
    }

    return false;
}

/**
 * Update List structure.
 * @param   l           List structure.
 * @param   value       New value.
 */
bool    list_insert(List *l, Value value) {
    if (list_search(l, value)) {
        return false;
    }

    while (l->next) {
        l = l->next;
    }

    if (l->size == l->capacity) {
        l->next = list_create(l->capacity, l->type);
        l       = l->next;
    }

    l->data[l->size++] = value;
    return true;
}

/**
 * Apply function to each value in List Structure.
 * @param   l           List structure.
 * @param   f           Function pointer.
 */
void	list_for_each(const List *l, ValueFunc f) {
    for (const List *curr = l; curr; curr = curr->next) {
        for (size_t v = 0; v < curr->size; v++) {
            f(curr->data[v], l->type);
        }
    }
}
            
/* Constants */

static const char *DATA = "california";

/* Functions */

void    value_print(const Value v, const Type t) {
    switch (t) {
        case LETTER: printf("v: %c\n", v.letter); break;
        case NUMBER: printf("v: %d\n", v.number); break;
    }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    List *l = list_create(4, LETTER);

    for (const char *c = DATA; *c; c++) {
        Value v = {*c};
        list_insert(l, v);
    }

    list_for_each(l, value_print);
    
    for (const char *c = DATA; *c; c++) {
        Value v = {*c};
        assert(list_search(l, v));
    }

    list_delete(l);

    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
