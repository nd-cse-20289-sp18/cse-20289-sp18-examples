/* list.h: Deque-based List */

#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Value Type */

typedef union {
    char    letter;
    int     number;
} Value;

typedef enum {
    LETTER,
    NUMBER,
} Type;

/* List Structure */

typedef struct List List;

struct List {
    size_t  size;
    size_t  capacity;
    Type    type;
    Value  *data;
    List   *next;
} ;

/* List Functions */

typedef void (*ValueFunc)(const Value v, const Type t);

List *	list_create(size_t capacity, Type type);
List *	list_delete(List *l);

bool    list_search(const List *l, const Value v);
bool    list_insert(List *l, Value v);
void	list_for_each(const List *l, ValueFunc f);

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
