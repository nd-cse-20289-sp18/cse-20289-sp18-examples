/* bitset.c */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* Bitset type definition */

typedef uint32_t Bitset;

/* Bitset functions */

Bitset	bitset_insert(Bitset b, char c) {
    int place = c - 'a';
    return b | (1<<place);
}

bool	bitset_search(Bitset b, char c) {
    int place = c - 'a';

    return b & (1<<place);
}

void	bitset_fprint(Bitset b, FILE *stream) {
    for (int place = 0; place < sizeof(Bitset)*8; place++) {
    	if (b & (1<<place)) {
    	    putc('a' + place, stdout);
	}
    }
    putc('\n', stdout);
}

/* Main execution */

const char *DATA = "california";

int main(int argc, char *argv[]) {
    Bitset b = 0;

    for (const char *c = DATA; *c; c++) {
    	b = bitset_insert(b, *c);
    }

    for (const char *c = DATA; *c; c++) {
    	assert(bitset_search(b, *c));
    }

    bitset_fprint(b, stdout);
    return 0;
}
