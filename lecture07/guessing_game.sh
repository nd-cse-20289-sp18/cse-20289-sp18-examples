#!/bin/sh

usage() {
    cat <<EOF
Usage: $(basename $0) [-h -n MIN -x MAX]
EOF
    exit $1
}

early_exit() {
    shuf <<EOF | head -n 1
Y So Serious?
The answer was $TARGET!
Fine, the number was $TARGET!
Ouch, that hurts!
EOF
    exit 1
}

trap early_exit HUP INT TERM

MIN=0
MAX=10

while [ "$#" -gt 0 ]; do
    case $1 in
	-n)
	    MIN=$2
	    shift
	    ;;
	-x)
	    MAX=$2
	    shift
	    ;;
	-h)
	    usage 0
	    ;;
	*)
	    usage 1
	    ;;
    esac
    shift
done

TARGET=$(shuf -i $MIN-$MAX | head -n 1)
GUESS=""
TRY=0

while [ "$GUESS" != "$TARGET" ]; do
    TRY=$(($TRY + 1))
    read -p "Guess $TRY? " GUESS
    if [ "$GUESS" -lt "$TARGET" ]; then
    	echo "Too low!"
    elif [ "$GUESS" -gt "$TARGET" ]; then
    	echo "Too high!"
    fi
done

echo "BOOM: You guessed $TARGET after $TRY tries!"
