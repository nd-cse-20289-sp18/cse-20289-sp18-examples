Lecture 07 - Shell Scripting
============================

# 0. How do I transfer files between machines?

    $ ip addr                       # Review: What is my IP address?

    $ nmap -v -Pn nyancat.dakko.us  # Review: What ports are open on a remote machine?

    $ nc nyancat.dakko.us 23        # Review: How do I connect to arbitrary service?
    $ reset

    $ curl http://nyancat.dakko.us  # Review: How do I make a HTTP request?

    $ ping -c 5 nyancat.dakko.us    # Review: How to test latency?

    # Show: Securely copy file to remote machine
    $ scp /etc/hosts student01.cse.nd.edu:hosts.txt
    $ sftp student01.cse.nd.edu
    put /etc/hosts

    # Show: Here document
    $ sftp student01.cse.nd.edu <<EOF
    cd  tmp
    put /etc/hosts
    EOF

# 1. How do you execute a shell script?

    $ tmux                          # Show: Terminal multiplexor

    $ touch hello.sh                # Show: Create script
    $ $EDITOR hello.sh
    echo "Hello, World"

    $ bash hello.sh                 # Discuss: How to execute script explicitly?
    $ sh hello.sh                   # Discuss: sh vs bash

    $ chmod +x hello.sh             # Discuss: How to execute script implicitly?
    $ ./hello.sh

    $ $EDITOR hello.sh              # Discuss: How does OS know what interpreter?
    #!/bin/sh
    echo "Hello, World"

    $ $EDITOR hello.sh
    #!/bin/cat
    echo "Hello, World"

# 2. How do you use variables?

    $ NAME="bill"                   # Discuss: How to set a variable?
    $ echo $NAME

    $ $EDITOR hello.sh              # Discuss: What happens if variable is not set?
    #!/bin/sh
    echo "Hello, $NAME"
    $ ./hello.sh

    $ export NAME                   # Discuss: How to export variable?
    $ ./hello.sh

    $ unset NAME                    # Discuss: How to remove variable?
    $ ./hello.sh

    $ $EDITOR hello.sh              # Discuss: How to set default value?
    #!/bin/sh                       # Discuss: valve's mistake
    NAME=${NAME:-Unknown}
    echo "Hello, $NAME"

    $ env NAME=vector ./hello.sh    # Discuss: How to temporary set variable?

# 3. How do you substitute the output of a command?

    $ date                          # Show: date command

    $ $EDITOR hello.sh              # Discuss: How to use output of a command?
    #!/bin/sh
    NAME=${NAME:-Unknown}
    echo "Hello, $NAME. Today is $(date)."

    $ $EDITOR hello.sh              # Discuss: Alternative syntax
    #!/bin/sh
    NAME=${NAME:-Unknown}
    DATE=`date`
    echo "Hello, $NAME. Today is ${DATE}."

# 4. How do you match based on patterns?

    $ $EDITOR hello.sh              # Discuss: Case statement
    #!/bin/sh
    NAME=${NAME:-Unknown}
    DATE=`date`
    case $NAME in
        bill|conor) GREETING=Sup    ;;
        happy)      GREETING=Bonjour;;
        *)          GREETING=Hello  ;;
    esac
    echo "$GREETING, $NAME. Today is ${DATE}."
    $ env NAME=bill ./hello.sh

# 5. How do you conditionally execute commands?

    $ stat hello.sh                 # Review: How to view information about a file?
    $ echo $?                       # Discuss: exit status, main function in C

    # Discuss: shortcircuit evaluation and boolean logic
    #           x && y
    #           x || y
    $ stat hello.sh && echo Success
    $ stat hello.sh || echo Success
    $ stat hello.sh > /dev/null && echo Success

    # Discuss: How to use conditional statement?
    $ if stat hello.sh > /dev/null; then
        echo The file exists
    fi

# 6. How do you check if a file exists?

    $ if test -e hello.sh; then     # Discuss: How to use test command?
        echo file exists
    fi

    $ which [                       # Discuss: How to use [ command?
    $ if [ -e hello.sh ]; then
        echo file exists
    fi

# 7. How do you do repeated execution?

    $ $EDITOR exists.sh             # Discuss: Reading 02 script
    #!/bin/sh
    if [ -e hello.sh ]; then        # Discuss: What happens if no spaces?
        echo file exists
    else
        echo file does not exist
    fi

    # Discuss: How to process command line arguments?
    #   command foo bar baz } $@ ($# number of arguments)
    #   $0      $1  $2  $3

    $ $EDITOR exists.sh
    #!/bin/sh
    for file in $@; do              # Discuss: for loop structure
        if [ -e "$file" ]; then     # Discuss: Good practice to quote
            echo $file exists
        else
            echo $file does not exist
        fi
    done
    $ ./exists.sh *                 # Discuss: globbing

    $ $EDITOR exists.sh
    #!/bin/sh
    while [ "$#" -gt 0 ]; do        # Discuss: while loop structure
        file=$1
        if [ -e "$file" ]; then
            echo $file exists
        else
            echo $file does not exist
        fi
        shift                       # Discuss: shift mechanic
    done
    $ ./exists.sh *

    $ $EDITOR exists.sh
    #!/bin/sh
    EXIT=0
    while [ "$#" -gt 0 ]; do
        file=$1
        if [ -e "$file" ]; then
            echo $file exists
        else
            echo $file does not exist
            EXIT=$(($EXIT + 1))     # Discuss: arithmetic
        fi
        shift
    done
    exit $EXIT
    $ ./exists.sh *
