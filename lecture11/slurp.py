#!/usr/bin/env python3

import multiprocessing
import os
import re
import requests

# Constants

BASE = 'http://proudtobe.nd.edu'
URL  = BASE + '/wallpapers/'

# Functions

def wget(url):
    p = os.path.basename(url)                   # Review: os.path.basename

    print('Downloading {} to {}'.format(url, p))
    r = requests.get(url)
    with open(p, 'wb') as f:                    # Review: Writing to a file, with statement
        f.write(r.content)

    return p

# Main Execution

if __name__ == '__main__':
    response   = requests.get(URL)              # Review: Requests, Regular expressions
    wallpapers = [BASE + asset for asset in re.findall('/assets/[^"]+', response.text)]

    # list(map(wget, wallpapers))               # Discuss: Why list?

    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    pool.map(wget, wallpapers)                  # Discuss: Timing
