#!/usr/bin/env python3

import sys

limit = int(sys.argv[1])
index = 0
while index < limit:
    print(index)
    index += 1
