Examples 11
===========

# 0. Iterators

## Examples

    # Iterate over list                         # Iterate over dict
    for n in [1, 2, 3, 4]:                      for k in os.environ:    # Show: .items()
        print(n)                                    print(k)

    # Iterate over string                       # Iterate over file
    for c in 'bill':                            for l in open('/etc/passwd'):
        print(c)                                    print(l)

## Data Streams

    >>> i = iter(range(5))                      # Explicit iterator
    >>> next(i)                                 # Get next item
    0
    ...
    >>> next(i)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    StopIteration

## Not Quite a List

    >>> r = reversed(range(5))                  # Reverse iterator
    >>> for i in r: print(i)
    >>> r[0]
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: 'range_iterator' object is not subscriptable

    >>> for i in r: print(i)                    # Discuss: Stream already consumed

## Common Iterators

    >>> x = range(10)                           # Stream of ints
    >>> r = reversed(x)                         # Reverse iterator

    >>> s = sorted(r)                           # Discuss: Sorted list (why?)

    >>> import itertools
    >>> c = itertools.cycle('ABCD')             # Infinite stream

## Trade-Offs: Range

    $ ./measure ./numbers1.py 10000000 > /dev/null
    limit, index = int(sys.argv[1]), 0		# While loop (Slow, Low)
    while index < limit:
       print(index)
	    index += 1

    $ ./measure ./numbers2.py 10000000 > /dev/null
    for n in list(range(int(sys.argv[1]))):	# List (Fast, High)
       print(n)

    $ ./measure ./numbers3.py 10000000 > /dev/null
    for n in range(int(sys.argv[1])): 		# Iterator (Fast, Low)
       print(n)


# 1. Generators

## Yield

    def triples(iterable):                      def triples(iterable):
        ts = []                                     for i in iterable:
        for i in iterable:                              yield n * 3
            ts.append(n * 3)
        return ts

    for t in triples([1, 2, 3]):                # Discuss: Trace coroutines
        print(t)

## Infinite Stream

    def cycle(iterable):                        # Discuss: Lazy evaluation
        while True:
            for i in iterable:
                yield i

## Trade-Offs: Reading 06

    $ seq 10000000 | ./measure ./evens.py     > /dev/null (Slow, High)

    $ seq 10000000 | ./measure ./evens_fp.py  > /dev/null (Slow, Low)

    $ seq 10000000 | ./measure ./evens_lc.py  > /dev/null (Fast, High)

    $ seq 10000000 | ./measure ./evens_gr.py  > /dev/null (Slow, Low)

    # Discuss: Generator Expression

    $ seq 10000000 | ./measure ./evens_ge.py  > /dev/null (Fast, Low)

# 2. Multiprocessing

    import multiprocessing, os, re, requests

    # Constants

    BASE = 'http://proudtobe.nd.edu'
    URL  = BASE + '/wallpapers/'

    # Functions

    def wget(url):
        p = os.path.basename(url)               # Review: os.path.basename

        print('Downloading {} to {}'.format(url, p))
        r = requests.get(url)                   
        with open(p, 'wb') as f:                # Review: Write to file, with statement
            f.write(r.content)

        return p

    # Main Execution

    if __name__ == '__main__':                  # Review: Guard
        response   = requests.get(URL)          # Review: Requests, Regular expressions
        wallpapers = [BASE + a for a in re.findall('/assets/[^"]+', response.text)]

        # list(map(wget, wallpapers))           # Discuss: Why list?

        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        pool.map(wget, wallpapers)              # Discuss: Timing
