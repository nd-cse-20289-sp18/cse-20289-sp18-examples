/* list.h: Deque-based List */

#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Value Type */

typedef union {
    char    letter;
    int     number;
} Value;

typedef enum {
    LETTER,
    NUMBER,
} Type;

/* List Structure */

typedef struct List List;

struct List {
    size_t  size;
    size_t  capacity;
    Type    type;
    Value  *data;
    List   *next;
} ;

/* List Functions */

List *	list_create(size_t capacity, Type type);
List *	list_delete(List *l);

bool    list_search(const List *l, const Value v);
void    list_insert(List *l, Value v);
void	list_fprint(const List *l, FILE *stream);

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
