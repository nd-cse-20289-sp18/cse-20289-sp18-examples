Examples 08
===========

# Pipeline: Powerful Pattern

Three volunteers to role-play and demonstrate a pipeline.

1. How many penguins are pink?

    read | filter pink | count

2. How many different colors of penguins do we have?

    read | sort | uniq

# Pipeline: Demonstration

1. How many instances of bash are running?

        $ ps aux | grep bash | grep -v grep | wc -l

2. How many different types of shells are being used?

        $ ps aux | grep -Ev '(grep|ssh|flush)' | grep -Eo '(bash|csh|tcsh)' | sort | uniq

    To see how many of each, do `uniq -c`

3. Who has the most processes?

        for user in $(ps aux | cut -d ' ' -f 1 | sort | uniq); do
            echo $(ps aux | grep -v grep | grep ^$user | wc -l) $user
        done | sort -rn

    Alternatively:

        ps aux | cut -d ' ' -f 1 | sort | uniq -c | sort -rn

4. Who has the most TROLL processes?

        ps aux | grep TROLL | cut -d ' ' -f 1 | sort | uniq -c | sort -rn

# Regular Expressions: Syntax

        $ echo "Bill" | grep -E '.'

        $ echo "Bill" | grep -E '.*'

        $ echo "Bill" | grep -E 'l*'

        $ echo "Conor" | grep -E 'o*'

        $ echo "Connor" | grep -E 'o?'

        $ echo "Connor" | grep -E 'n{2}'

        $ echo "Connor" | grep -E '[on]+'

        $ echo "Connor" | grep -E '[^on]'

        $ echo "Connor" | grep -E '^C'

        $ echo "Connor" | grep -E 'r$'

        $ echo "Conor" | grep -E '(o).*\1'

# Regular Expressions: Examples

1. All the strings                                      `.*`

2. Only hat, cat                                        `^[ch]at$`

3. All words with cat                                   `cat`

4. All words with two t's                               `t{2}`

5. Words that don't start with b                        `^[^b]`

6. All words with a vowel followed by a t               `[aeiou]t`

7. All words that begin and end with the same letter    `^([a-z]).*\1$`

8. All the PJ masks!                                    `(catboy|gecko|owlette)`

# Activity: Contact Harvesting

1. Extract all the phone numbers

        $ curl -sL "http://yld.me/aBG"
                | grep -Eo '[0-9]{3}-[0-9]{3}-[0-9]{4}' | sort | uniq

2. Extract all the phone numbers

        $ curl -sL "http://yld.me/aBG"
                | grep -Eo '[[:alnum:]]+@[[:alnum:].]+' | sort | uniq

3. Extract all the "Professorial" positions

        $ curl -sL "http://yld.me/aBG"
                | grep -Eo '[^>]+Prof[^<]+' | sort | uniq

# Activity: Faculty Information

1. How many faculty have Ph.D's? M.S.'s?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Ph\.D|M\.S).*/\1/p'
                | sort | uniq -c

        $ curl -sL http://cse.nd.edu/people/faculty
                | awk 'match($0, /(M\.S|Ph\.D)/, m) {counts[m[1]]++}
                  END { for (count in counts) { print counts[count], count } }'

2. How many faculty have Ph.D's or M.S's from Notre Dame?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Ph\.D|M\.S).*Notre Dame.*/\1/p'
                | sort | uniq -c

3. What year did most of the faculty graduate?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Ph\.D|M\.S).*([0-9]{4}).*/\2/p'
                | sort | uniq -c | sort -rn

4. How many professional specialists?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Professional Specialist).*/\1/p'
                #| grep -Eo 'Professional Specialist'
