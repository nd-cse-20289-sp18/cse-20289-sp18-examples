Examples 10
===========

# 0. Imperative vs Functional Programming

    Imperative                                  Functional
    ----------                                  ----------

    - Programs as a sequence of instructions    - Programs as composition of functions
    - Change state and mutate data              - Minimize state and don't mutate data
    - Explicitly describe changes               - Transform streams

    Note: Unix Philosophy (1. do one thing well, 2. composition, 3. communicate)

# 1. Map, Filter, Reduce

## Discuss: Map

    triples = []                                triples = map(lambda n: n * 3, numbers)
    for n in numbers:                           # Mention: generator vs list
        triples.append(n * 3)

## Discuss: Lambda

    add = lambda a, b: a + b                    # Define function
    add(1, 3)                                   # Call function
    map(add, [1, 2, 3], [4, 5, 6])              # Pass function as argument
    map(lambda a, b: a + b, [1, 2, 3], [4, 5, 6])

## Discuss: Filter

    odds = []                                   odds = filter(lambda t: t % 2, triples)
    for t in triples:
        if t % 2:
        odds.append(t)

    # Discuss: Reduce

    total = 0                                   from functools import reduce
    for o in odds:                              total = reduce(lambda b, n: b + n, odds)
        total += o

# 2. List comprehension

## Discuss: Map -> List Comprehension

    triples = map(lambda n: n * 3, numbers)     triples = [n * 3 for n in numbers]

## Discuss: Filter -> List Comprehension

    odds = filter(lambda t: t % 2, triples)     odds = [t for t in triples if t % 2]

## Discuss: Reduce -> None

    total = reduce(lambda b, n: b + n, odds)    total = sum(odds)









# 3. Demonstration: exam01.csv

## Discuss: Map

    >>> import requests
    >>> lines  = requests.get('https://yld.me/FjI').text.splitlines()
    >>> data   = map(lambda l: l.split(','), lines)

    >>> data            # Generator
    >>> list(data)      # List

    >>> data   = map(lambda l: list(map(float, l.split(','))), lines)

    >>> scores = map(lambda l: sum(map(float, l.split(','))), lines)

## Discuss: Filter

    >>> scores = list(map(lambda l: sum(map(float, l.split(','))), lines))

    >>> As = filter(lambda s: s >= 27, scores)
    >>> len(list(As))

    >>> Bs = filter(lambda s: 24 <= s < 27, scores)
    >>> len(list(Bs))

## Discuss: Reduce

    >>> Max = reduce(lambda a, b: a if a > b else b, scores)

## Discuss: List Comprehensions

    >>> scores = [sum(map(float, l.split(','))) for l in lines]
    >>> As     = [s for s in scores if s >= 27]
    >>> Bs     = [s for s in scores if 24 <= s < 27]
    >>> Max    = max(scores)

# 4. Demonstration: demographics.csv

    import pprint
    import requests

    lines = requests.get('https://yld.me/lNZ').text.splitlines()
    years = [year for year in lines[0].split(',') if year]
    data  = {year: {} for year in years}        # Discuss: Dictionary comprehensions

    for row in map(lambda s: s.split(','), lines[1:]):
        for index, year in enumerate(years):
            gender = row[2*index]
            ethnic = row[2*index + 1]
            if gender:                          # Discuss: collections.defaultdict
                data[year][gender] = data[year].get(gender, 0) + 1
            if ethnicity:
                data[year][ethnic] = data[year].get(ethnic, 0) + 1

    # Discuss: Pretty printing
    pprint.pprint(data, width=100)
