/* heap.c */

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char *str_title(const char *s) {
    char *t = malloc(strlen(s));
    strncpy(t, s, strlen(t));

    char *c = t;
    while (*c) {
	*c = toupper(*c);
	while (*c && !isspace(*c++));
	while (*c &&  isspace(*c++));
    }

    return t;
}


int main(int argc, char *argv[]) {
    int *h = malloc(INT_MAX);

    // Doesn't fail on Linux
    if (h == NULL) {
    	fprintf(stderr, "malloc failed: %s\n", strerror(errno));
    	return EXIT_FAILURE;
    }

    // Show ps ux: virtual vs physical memory
    sleep(10);

    for (int i = 1; i <= argc; i++) {
	char *t = str_title(argv[i]);
	puts(t);
    }

    return EXIT_SUCCESS;
}
