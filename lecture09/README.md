Examples 09
===========

# 0. Python Interpreter                         # 1. Python Scripts

    $ which python                              $ $EDITOR hello.py
    $ python                                    #!/usr/bin/env python3
                                                print('Hello, World')
    >>> print('Hello, world')
                                                $ chmod +x hello.py
                                                $ ./hello.py

# 2. Variables, Types, Expressions

    >>> 'conor'                                 >>> name = 'conor'
    >>> type('conor')                           >>> type(name)
    >>> dir('conor')                            >>> dir(name)
    >>> 'conor'.upper()

    >>> help(name.replace)                      # Discuss: help
    >>> name.replace('n', 'l')                  # Discuss: side-effects/immutable
    >>> name

    >>> name = name.replace('n', 'l')           # Discuss: assignment
    >>> name

    >>> number = '1123'                         >>> number = int(number)
    >>> type(number)                            >>> type(number)

# 3. Flow Control

    $ $EDITOR flow.py                           # Discuss: if statement
    #!/usr/bin/env python3

    import random
    n = random.randint(0, 10)                   # Discuss: print(n, ...)
    if n < 5:                                   # Discuss: print('{} ...'.format(n))
        print('less than 5')                    # Discuss: elif
    else:
        print('greater than or equal to 5')

    $ $EDITOR flow.py                           # Discuss: while loop
    #!/usr/bin/env python3

    import random
    n = random.randint(0, 10)

    while n != 5:
        print(n)                                # Discuss: infinite loop

    $ $EDITOR flow.py                           # Discuss: for loop
    #!/usr/bin/env python3

    for number in [9, 2, 8, 6, 7]:              # Discuss: range()
        print(number)

    $ $EDITOR flow.py                           # Discuss: list
    #!/usr/bin/env python3

    TAS = ['conor', 'alison', 'john']

    for ta in TAS:                              # Discuss: enumerate
        print(ta)

# 4: Fizz Buzz

    $ $EDITOR flow.py                           # Discuss: fizz buzz
    #!/usr/bin/env python3

    for number in range(1, 101):
        if number % 3 == 0 and number % 5 == 0:
            print('FizzBuzz')
        elif number % 3 == 0:
            print('Fizz')
        elif number % 5 == 0:
            print('Buzz')
        else:
            print(number)

    $ $EDITOR flow.py                           # Discuss: functions
    #!/usr/bin/env python3
    import sys                                  # Discuss: command-line arguments

    def fizzbuzz(start=1, stop=101):
        for number in range(start, stop):
            if number % 3 == 0 and number % 5 == 0:
                print('FizzBuzz')
            elif number % 3 == 0:
                print('Fizz')
            elif number % 5 == 0:
                print('Buzz')
            else:
                print(number)

    if __name__ == '__main__':                  # Discuss: guard
        fizzbuzz(int(sys.argv[1]), int(sys.argv[2]))


































# 5: Data Structures & I/O

    $ $EDITOR 1337speak.py
    import sys

    MAPPING = {
        'a': '4',
        'e': '3',
        'i': '1',
        'o': '0',
    }

    for line in sys.stdin:
        result = ''
        for letter in phrase.rstrip():
            result += MAPPING.get(letter, letter)   # Discuss: get method
        print(result)

    # Discuss: Naive version with lists, tuples, input()
    # Discuss: Iterating over list, result string, logic, complexity
    # Discuss: Better version with dict, get()
    # Discuss: Traditional version with sys.stdin, rstrip()

# 6: Data Structures

    $ $EDITOR spellcheck.py
    import sys

    # Constants

    PATH = '/usr/share/dict/words'

    # Functions

    def load_words_list(path=PATH):
        words = []
        for word in open(path):                 # Discuss: Garbage collection, exceptions
            words.append(word.strip())          # Discuss: str.strip
        return words

    def load_words_dict(path=PATH):
        words = {}
        for word in open(path):
            words[word.strip()] = True
        return words

    def load_words_set(path=PATH):
        words = set()
        for word in open(path):
            words.add(word.strip())
        return words

    # Main Execution

    if __name__ == '__main__':                  # Discuss: Guard
        #words = load_words_list()
        words = load_words_dict()
        #words = load_words_set()

        for line in sys.stdin:                  # Discuss: Complexity
            for word in line.strip().split():
                if word not in words:
                    print('{} is misspelled!'.format(word))


# 7: Regular Expressions & Requests

    # Discuss: Start and use notebook for prototyping
    $ jupyter notebook --ip studentXX.cse.nd.edu --port 9000 --no-browser

    def zipcode(target=None, state='Indiana'):
        url      = 'http://www.zipcodestogo.com/{}/'.format(state)
        response = requests.get(url)
        regex    = '<a href="http://www.zipcodestogo.com/([^/]+)/[A-Z]{2}/([0-9]{5})/">'

        for city, zipcode in re.findall(regex, response.text):
            if target is None or target == city:
                print(zipcode)

    # Discuss: requests.get(), re.findall(), None
